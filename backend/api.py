from rest_framework import routers
from meetings.api_views import MeetingViewset
from django.urls import path


router = routers.DefaultRouter()

router.register(r'meetings', MeetingViewset, basename='meeting')


