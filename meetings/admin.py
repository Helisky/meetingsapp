from django.contrib import admin
from .models import Meeting

class MeetingAdmin(admin.ModelAdmin):
    list_display = ('id', 'meeting_name', 'start_time', 'end_time', 'user_id')
    list_display_links = ('id', 'meeting_name')
    search_fields = ('meeting_name', 'start_time', 'end_time', 'user_id')
    list_per_page = 25


admin.site.register(Meeting, MeetingAdmin)
