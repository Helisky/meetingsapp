from rest_framework import viewsets
from .models import Meeting
from .serializers import MeetingSerializer
from rest_framework import mixins
from rest_framework import serializers
from .utils import validate_datetimes, validate_start_time, validate_end_time, validate_specific_user, validate_available_users
from django.contrib.auth.models import User
from rest_framework.response import Response
from rest_framework import status
from .exceptions import CustomValidationError


class MeetingViewset(
        mixins.CreateModelMixin,
        viewsets.GenericViewSet):
    queryset = Meeting.objects.all()
    serializer_class = MeetingSerializer

    def perform_create(self, serializer):

        users = self.request.data['users']
        validate_datetimes(self, self.request.data)
        validate_start_time(self, self.request.data['start_time'])
        validate_end_time(self, self.request.data['end_time'])
        try:
            users = [int(x) for x in users.split(',')]
        except:
            raise CustomValidationError({
                'users': "Users must be a comma separated list of integers."
            }, code='invalid_request')

        validate_available_users(self, users)

        # response 200 ok
        serializer.save(users=users)

    def create(self, request, *args, **kwargs):
        """create a meeting

        Args:
            request (): request from client

        Returns:
            _type_: 200 ok if meeting is created, 400 bad request if meeting is not created
        """
        
        serializer = self.get_serializer(data=request.data)
        
        if serializer.is_valid():
            self.perform_create(serializer)
            return Response(
                {'detail': 'Meeting has been Booked'},
                status=status.HTTP_200_OK)
        else:
            return Response(
                {'detail': 'Meeting can not been Booked'},
                status=status.HTTP_400_BAD_REQUEST)
