from rest_framework.exceptions import ValidationError

class CustomValidationError(ValidationError):
    def __init__(self, detail:dict, code=None):
        """Custom validation error

        Args:
            detail (dict): dictionary with the error details
            code (str, optional): error code. Defaults to None.
        """
        # Mensaje de error personalizado
        message = "The meeting can not been booked"
        # Llama al constructor de la superclase con el mensaje personalizado
        super().__init__({code: message, 'details': detail})
