from django.db import models
from django.contrib.auth.models import User

class Meeting(models.Model):
    meeting_name = models.CharField(max_length=255)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    user_id = models.ForeignKey(User, on_delete=models.CASCADE, related_name='meetings')
    
    def __str__(self):
        return '{0}-{1}'.format(self.id, self.meeting_name)
    
    
    class Meta:
        db_table = 'meeting'
        verbose_name = 'meeting'
        verbose_name_plural = 'meetings'