from rest_framework import serializers
from django.contrib.auth.models import User
from .models import Meeting
from rest_framework import status
from rest_framework.response import Response

class MeetingSerializer(serializers.ModelSerializer):
    users = serializers.CharField(required=True, write_only=True)
    user_id = serializers.ReadOnlyField(source='user.username')
    # user_id = serializers.IntegerField(required=True)
    class Meta:
        model = Meeting
        fields = ('id', 'meeting_name', 'start_time', 'end_time', 'user_id', 'users')
        
    
    def create(self, validated_data):

        # remove users from validated_data
        users = validated_data.pop('users')
        
        
        for user in users:
            specific_user = User.objects.filter(id=user).first()
            
            meeting = Meeting.objects.create(
                meeting_name=validated_data['meeting_name'],
                start_time=validated_data['start_time'],
                end_time=validated_data['end_time'],
                user_id=specific_user
            )
            meeting.save()
        return meeting
        


