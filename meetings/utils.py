from rest_framework import serializers
from datetime import datetime
from django.contrib.auth.models import User
from .models import Meeting
from .exceptions import CustomValidationError
from django.db.models import Q


def validate_datetimes(self, data: dict):
    """validate if the start_time is before end_time

    Args:
        data (dict): data from request

    Raises:
        CustomValidationError: set error message if start_time is before end_time
    """
    if data['start_time'] > data['end_time']:
        raise CustomValidationError({
            'start_time': "Start time must be before end time."
        }, code='invalid_request')


def validate_start_time(self, value: str):
    """validate if the start_time is in the future

    Args:
        value (str): start_time from request

    Raises:
        CustomValidationError: set error message if start_time is in the past
    """

    # convert str to datetime
    value = datetime.strptime(value, '%Y-%m-%dT%H:%M')

    if value < datetime.now():
        raise CustomValidationError({
            'start_time': 'Start time must be in the future.'
        }, code='invalid_request')


def validate_end_time(self, value: str):
    """validate if the end_time is in the future

    Args:
        value (str): end_time from request

    Raises:
        CustomValidationError: set error message if end_time is in the past
    """
    # convert str to datetime
    value = datetime.strptime(value, '%Y-%m-%dT%H:%M')
    if value < datetime.now():
        raise CustomValidationError({
            'end_time': 'End time must be in the future.'
        }, code='invalid_request')


def validate_specific_user(self, data: User):
    """validate if the user is available

    Args:
        data (User): user object

    Raises:
        CustomValidationError: set error message if user is not available
    """
    
    # validate if user has a meeting during datetime range
    some_meeting = Meeting.objects.filter(
        Q(user_id=data.id) &
        (
            (Q(start_time__gte=self.request.data['start_time']) & Q(
                end_time__lte=self.request.data['end_time'])
            ) |
            (Q(start_time__lte=self.request.data['start_time']) & Q(
                end_time__gte=self.request.data['end_time'])
            )
        )
    )

    if some_meeting.exists():
        raise CustomValidationError({
            'start_time': f"User {data.username} not available."
        }, code='invalid_request')


def validate_available_users(self, data: list):
    """validate if the users exist and available

    Args:
        data (list): list of user ids

    Raises:
        CustomValidationError: set error message if user is not available
        CustomValidationError: set error message if user not found
    """
    if data == []:
        raise CustomValidationError({
            'user_id': f"User {data.username} not available."
        }, code='invalid_request')

    elif len(data) >= 1:
        for user in data:
            specific_user = User.objects.filter(id=user).first()
            if specific_user is None:
                raise CustomValidationError({
                    'user_id': f"User with ID {data} not found."
                }, code='invalid_request')
            else:
                validate_specific_user(self, specific_user)
