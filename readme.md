# Meetings

## clonar repositorio

```bash
    git clone https://gitlab.com/Helisky/meetingsapp.git
```

## ir al directorio y crear el entorno virtual

```bash
    python -m virtualenv env
```

## activar entorno virtual

```bash
    # para linux
    source/env/bin/activate
    # windows
    env/Scripts/activate.bat
```

## instalar requerimientos

```bash
    pip install -r requirements.txt
```

## Crear base de datos en postgres

## Crear y agregar credenciales en .env file

-Agregar variables de entorno encontradas en .env.example file

## levantar la aplicacion

```bash
    ./manage.py makemigrations
    ./manage.py migrate
    ./manage.py runserver
```
## crear super usuario
```
    ./manage.py createsuperuser
```

## RUTAS

([localhost]:[puerto])

([localhost]:[puerto])/admin
([localhost]:[puerto])/api/